"""
Flask web site with vocabulary matching game
(identify vocabulary words that can be made
from a scrambled string)
"""

import flask
import logging

# Our own modules
from letterbag import LetterBag
from vocab import Vocab
from jumble import jumbled
import config

###
# Globals
###
app = flask.Flask(__name__)

CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY  # Should allow using session variables

WORDS = Vocab(CONFIG.VOCAB)#credentials look vocac that it is a path to first_grade.txt words is a object with several methods.

###
# Pages
###
@app.route("/")
@app.route("/index")
def index():
    """The main page of the application"""
    flask.g.vocab = WORDS.as_list()#lista
    flask.session["target_count"] = min(
        len(flask.g.vocab), CONFIG.SUCCESS_AT_COUNT)#numero de palabras a encontrar
    flask.session["jumble"] = jumbled(
        flask.g.vocab, flask.session["target_count"])#list of words and number of words
    flask.session["matches"] = []
    app.logger.debug("Session variables have been set")
    assert flask.session["matches"] == []
    assert flask.session["target_count"] > 0
    app.logger.debug("At least one seems to be set correctly")
    return flask.render_template('vocab.html')

@app.route("/_ajax")#from minijax
def countem():
    text = flask.request.args.get("text", type=str)
    jumble = flask.session["jumble"]#from session
    matches = flask.session.get("matches", [])  # Default to empty list
    in_jumble = LetterBag(jumble).contains(text)#check if a word is inside.
    matched = WORDS.has(text)#check if the word has been already guessed
    if matched and in_jumble and not (text in matches):#New Match
        rslt = {"response": "Match"}
        app.logger.debug("MATCH!!")
        matches.append(text)
        flask.session["matches"] = matches
    elif text in matches:#Already Found
        app.logger.debug("Already found.")
        rslt = {"response": "Already"}
    elif len(text) > 7:
        rslt = {"response": "many"}
    elif not matched:
        app.logger.debug("No in Vocab")
        rslt = {"response": "No"}
    elif not in_jumble:
        rslt = {"response": "No"}
    else:
        app.logger.debug("This case shouldn't happen!")
        rslt = {"response": "No"}
        assert False  # Raises AssertionError

    if len(matches) >= flask.session["target_count"]:
       rslt = {"response": "end"}
       return flask.jsonify(result=rslt)
    else:
       return flask.jsonify(result=rslt)

#################
# Functions used within the templates
#################

@app.template_filter('filt')
def format_filt(something):
    """
    Example of a filter that can be used within
    the Jinja2 code
    """
    return "Not what you asked for"

###################
#   Error handlers
###################

@app.errorhandler(404)
def error_404(e):
    app.logger.warning("++ 404 error: {}".format(e))
    return flask.render_template('404.html'), 404

@app.errorhandler(500)
def error_500(e):
    app.logger.warning("++ 500 error: {}".format(e))
    assert not True  # I want to invoke the debugger
    return flask.render_template('500.html'), 500

@app.errorhandler(403)
def error_403(e):
    app.logger.warning("++ 403 error: {}".format(e))
    return flask.render_template('403.html'), 403

###

if __name__ == "__main__":
    if CONFIG.DEBUG:
        app.debug = True
        app.logger.setLevel(logging.DEBUG)
        app.logger.info(
            "Opening for global access on port {}".format(CONFIG.PORT))
        app.run(port=CONFIG.PORT, host="0.0.0.0")

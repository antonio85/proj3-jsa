start:
		docker start $(n)
stop:
		docker stop $(n)
bash:
		docker exec -t -i $(n) /bin/bash
build:
		docker build -t $(n) .
run:
		docker run -t -d -p 5000:5000 $(n)

all:
		docker build -t $(n) .
		docker run -t -d -p 5000:5000 $(n)
git:
		git add *
		git commit -m "$(n)"
		git push
